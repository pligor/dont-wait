import sbt._

import Keys._
import AndroidKeys._

object General {
  val settings = Defaults.defaultSettings ++ Seq(
    javacOptions ++= Seq("-source", "1.6", "-target", "1.6"),
    scalacOptions ++= Seq("-deprecation", "-unchecked"),
    name := "WiFiMe",
    version := "0.1",
    versionCode := 1,
    scalaVersion := "2.9.2",
    platformName in Android := "android-10"
  )

  val proguardSettings = Seq(
    useProguard in Android := true,
    proguardOption in Android :=
      """-keep class scala.Function1
        |
        |-keep class android.bluetooth.BluetoothDevice {
        | *** fetchUuidsWithSdp(...);
        | *** getUuids(...);
        |}
      """.stripMargin
  )

  lazy val fullAndroidSettings =
    General.settings ++
      AndroidProject.androidSettings ++
      TypedResources.settings ++
      proguardSettings ++
      AndroidManifestGenerator.settings ++
      AndroidMarketPublish.settings ++ Seq(
      //libraryDependencies += "org.scalatest" %% "scalatest" % "1.8" % "test",
      libraryDependencies ++= Seq(
        "commons-io" % "commons-io" % "2.4",
        "org.apache.commons" % "commons-math3" % "3.1.1"
      ),
      keyalias in Android := "gpligor"
    )
}

object AndroidBuild extends Build {
  lazy val main = Project(
    "WiFiMe",
    file("."),
    settings = General.fullAndroidSettings
  )

  lazy val tests = Project(
    "tests",
    file("tests"),
    settings = General.settings ++
      AndroidTest.androidSettings ++
      General.proguardSettings ++ Seq(
      name := "WiFiMeTests"
    )
  ) dependsOn main
}
