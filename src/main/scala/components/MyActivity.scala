package components

import android.app.Activity
import android.view.ViewGroup
import android.os.Bundle

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyActivity extends Activity {
  implicit val context = this;
  implicit lazy val resources = getResources;

  protected lazy val grabString: (Int) => String = getResources.getString;

  def getContentLayout = findViewById(android.R.id.content).asInstanceOf[ViewGroup].getChildAt(0);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    /*if (!isSDcardAvailable) {
      Toast.makeText(
        getApplicationContext,
        String.format(grabString(R.string.no_sd_card_warning), grabString(R.string.app_name)),
        Toast.LENGTH_LONG
      ).show();
      finish();
    }
    SoundWrapper.init;*/
  }
}
