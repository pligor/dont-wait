package components.helpers

import android.bluetooth.BluetoothAdapter
import android.nfc.NfcAdapter
import android.content.Context
import android.util.{Xml, AttributeSet}
import org.xmlpull.v1.XmlPullParser
import android.content.res.Resources
import android.net.ConnectivityManager
import android.widget.Toast
import android.app.ActivityManager
import scala.collection.JavaConverters._

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AndroidHelper {
  def getAppNameByPID(pid: Int)(implicit context: Context): Option[String] = {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE).asInstanceOf[ActivityManager];

    val processInfo = manager.getRunningAppProcesses.asScala.find(_.pid == pid);
    if (processInfo.isDefined) {
      Some(processInfo.get.processName);
    } else {
      None;
    }
  }

  def showToast(resourceId: Int)(implicit context: Context) {
    Toast.makeText(context, resourceId, Toast.LENGTH_LONG).show();
  }

  def getAttributeSetFromXml(xmlId: Int, tagName: String, resources: Resources): AttributeSet = {
    /*var state = XmlPullParser.END_DOCUMENT;
    do {
      try {
        state = xmlPullParser.next();
        if (state == XmlPullParser.START_TAG) {
          if (xmlPullParser.getName contains "CardView") {
            Logger(xmlPullParser.getName);
            attrs = Xml.asAttributeSet(xmlPullParser);

            state = XmlPullParser.END_DOCUMENT; //same as break :P
          }
        }
      }
    } while (state != XmlPullParser.END_DOCUMENT);*/

    /**
     * The good thing for being an internal function is that we don't need to pass tagName as a ref
     */
    def getAttributeSet(xmlPullParser: XmlPullParser /*, tagName: String*/): AttributeSet = {
      val state = xmlPullParser.next();
      if (state == XmlPullParser.START_TAG &&
        (xmlPullParser.getName contains tagName)) {
        Xml.asAttributeSet(xmlPullParser);
      }
      else {
        if (state == XmlPullParser.END_DOCUMENT) null;
        else getAttributeSet(xmlPullParser /*, tagName*/);
      }
    }

    getAttributeSet(resources.getXml(xmlId) /*, tagName*/);
  }

  def isWifiConnected(implicit context: Context): Boolean = {
    context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager].
      getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected;
  }

  def isSDcardAvailable: Boolean = {
    android.os.Environment.getExternalStorageState == android.os.Environment.MEDIA_MOUNTED
  }

  def isBluetoothSupported: Boolean = {
    BluetoothAdapter.getDefaultAdapter match {
      case null => false;
      case x: BluetoothAdapter => true;
    }
  }

  /**
   * Note: getDefaultAdapter with no context parameter is deprecated
   */
  def isNFCsupported(implicit context: Context): Boolean = {
    NfcAdapter.getDefaultAdapter(context) match {
      case null => false;
      case x: NfcAdapter => true;
    }
  }
}
