package components.helpers

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ByteSize {
  private val bitsPerByte = 8;

  val Int = Integer.SIZE / bitsPerByte;
  val Boolean = 1;
  val Long = java.lang.Long.SIZE / bitsPerByte;
  val MD5 = 16;
}
