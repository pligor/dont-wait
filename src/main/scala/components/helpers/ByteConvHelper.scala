package components.helpers

import java.nio.ByteBuffer

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ByteConvHelper {
  def boolean2byte(value: Boolean): Byte = {
    if (value) 1.toByte
    else 0.toByte;
  }

  def arrayByte2boolean(array: Array[Byte]): Boolean = {
    require(array.length == 1);
    val byte = array.head;
    if (byte == 1.toByte) {
      true;
    }
    else if (byte == 0.toByte) {
      false;
    }
    else throw new IllegalArgumentException;
  }

  def boolean2arrayByte(value: Boolean): Array[Byte] = {
    val bb = ByteBuffer.allocate(ByteSize.Boolean);
    bb.put(boolean2byte(value));
    bb.array();
  }

  def int2arrayByte(x: Int): Array[Byte] = {
    val bb = ByteBuffer.allocate(ByteSize.Int);
    bb.putInt(x);
    bb.array();
  }

  def arrayByte2int(array: Array[Byte]): Int = {
    require(array.length == ByteSize.Int);
    ByteBuffer.wrap(array).getInt;
  }

  def long2arrayByte(x: Long): Array[Byte] = {
    val bb = ByteBuffer.allocate(ByteSize.Long);
    bb.putLong(x);
    bb.array();
  }

  def arrayByte2long(array: Array[Byte]): Long = {
    require(array.length == ByteSize.Long);
    ByteBuffer.wrap(array).getLong;
  }
}
