package components.generic

import android.util.Log

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object log {
  def log(msg: Any): Int = {
    Log.i("pl", msg.toString);
  }

  def apply(msg: Any): Int = log(msg);
}
