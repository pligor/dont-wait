package com.pligor.wifime

import android.nfc.NdefMessage
import java.lang.String
import org.apache.commons.io.Charsets
import components.helpers.ByteConvHelper._
import myandroid.WifiHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object WiFiMe {
  val PACKAGE_NAME = "com.pligor.wifime";
  val MIME_TYPE = "application/" + PACKAGE_NAME;

  def getSSID(ndefMessage: NdefMessage): String = {
    val ssidRecord = ndefMessage.getRecords.apply(0);
    new String(ssidRecord.getPayload, Charsets.US_ASCII);
  }

  def getPass(ndefMessage: NdefMessage): String = {
    val passRecord = ndefMessage.getRecords.apply(1);
    new String(passRecord.getPayload, Charsets.US_ASCII);
  }

  def getProtocol(ndefMessage: NdefMessage): WifiProtocol.Value = {
    val protocolRecord = ndefMessage.getRecords.apply(2);
    WifiProtocol(arrayByte2long(protocolRecord.getPayload).toInt);
  }

  def getMessage(ndefMessage: NdefMessage): String = {
    val messageRecord = ndefMessage.getRecords.apply(3);
    new String(messageRecord.getPayload, Charsets.UTF_8);
  }
}
