package activities

import _root_.android.app.Activity
import _root_.android.os.Bundle
import com.pligor.wifime.{TR, R, TypedActivity}

class MainActivity extends Activity with TypedActivity {
  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)
    setContentView(R.layout.activity_main);

    findView(TR.textview).setText("hello, world!");
  }
}
