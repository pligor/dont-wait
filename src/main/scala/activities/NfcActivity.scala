package activities

import android.app.Activity
import components.MyActivity
import android.nfc.{NdefMessage, NfcAdapter}
import android.os.Bundle
import android.net.wifi.WifiManager
import android.content.{Intent, BroadcastReceiver, IntentFilter, Context}
import com.pligor.wifime.{WiFiMe, R, TR, TypedActivity}
import components.generic.{log, WriteOnce}
import myandroid.WifiHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class NfcActivity extends Activity
with TypedActivity
with MyActivity {
  private lazy val nfcResultTextView = findView(TR.nfcResultTextView);

  private lazy val wifiManager = getSystemService(Context.WIFI_SERVICE).asInstanceOf[WifiManager];

  private val ndefMessage = new WriteOnce[NdefMessage];

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_nfc);

    val intent = getIntent;
    val intentTypeOption = Option(intent.getType);

    if (intentTypeOption.isDefined && intentTypeOption.get == WiFiMe.MIME_TYPE) {
      val ndefMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

      if (ndefMessages.length == 1) {
        ndefMessage setValue ndefMessages(0).asInstanceOf[NdefMessage];

        nfcResultTextView.setText(WiFiMe.getMessage(ndefMessage()));

        val intentFilter = new IntentFilter();
        //intentFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);

        if (wifiManager.isWifiEnabled) {
          connectToNetwork();
        } else {
          wifiManager.setWifiEnabled(true);
        }
      } else {
        nfcResultTextView.setText("invalid nfc tag");
      }
    } else {
      nfcResultTextView.setText("invalid nfc tag");
    }
  }

  private def connectToNetwork() {
    val ssid = WiFiMe.getSSID(ndefMessage());
    log log "ssid: " + ssid;
    val pass = WiFiMe.getPass(ndefMessage());
    log log "pass: " + pass;
    val protocol = WiFiMe.getProtocol(ndefMessage());
    log log "protocol: " + protocol;
    connectToWifiNetwork(ssid, pass, protocol);
  }

  private object broadcastReceiver extends BroadcastReceiver {
    def onReceive(context: Context, intent: Intent) {
      val action = intent.getAction;
      action match {
        case WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION => {
          val wifiConnected = intent.getBooleanExtra(WifiManager.EXTRA_SUPPLICANT_CONNECTED, false);
          if (wifiConnected) {
            log log "wifi just got connected";
          } else {
            log log "wifi just got disconnected";
          }
        }
        case WifiManager.WIFI_STATE_CHANGED_ACTION => {
          val curState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);

          curState match {
            case WifiManager.WIFI_STATE_ENABLED => {
              log log "wifi just got enabled";
              connectToNetwork();
            }
            case WifiManager.WIFI_STATE_DISABLED => {
              log log "wifi just got disabled";
            }
            case WifiManager.WIFI_STATE_DISABLING |
                 WifiManager.WIFI_STATE_ENABLING |
                 WifiManager.WIFI_STATE_UNKNOWN => {}
          }
        }
      }
    }
  }

}
