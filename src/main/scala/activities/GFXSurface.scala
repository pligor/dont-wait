package activities

import android.app.Activity
import android.content.Context;
import android.graphics._
import android.os.Bundle
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.View.OnTouchListener
import com.pligor.wifime.{TR, TypedActivity, R}
import components.generic.log
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D
import android.graphics.drawable.BitmapDrawable

class GFXSurface extends Activity with TypedActivity
with OnTouchListener {
  private var touchPosition = Vector2D.NaN;
  private var pressedPosition = Vector2D.NaN;
  private var releasedPosition = Vector2D.NaN;
  private var animationPosition = Vector2D.NaN;
  private var scaledVector = Vector2D.NaN;

  private val period = 50;
  private val speedFactor = 1500.0;
  private val fadeFactor = 300;

  private lazy val fadePaint = new Paint();

  private lazy val greenball = BitmapFactory.decodeResource(getResources, R.drawable.greenball);

  //private lazy val androidBitmap = BitmapFactory.decodeResource(getResources, R.drawable.ic_launcher);

  private lazy val mainSurfaceView = findView(TR.mainSurfaceView);

  private lazy val mySurface = new MyBringBackSurface(mainSurfaceView);

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_testing);

    mainSurfaceView.setOnTouchListener(this); //set on touch listener
  }

  override def onResume() {
    super.onResume();
    mySurface.resume();
    log log getResources.getColor(R.color.background);
  }

  override def onPause() {
    super.onPause();
    mySurface.pause();
  }

  def onTouch(view: View, motionEvent: MotionEvent): Boolean = {

    touchPosition = new Vector2D(motionEvent.getX, motionEvent.getY);

    motionEvent.getAction match {
      case MotionEvent.ACTION_DOWN => {
        //we need to keep a reference to create the vector later
        pressedPosition = touchPosition;

        //reset all variables who play role
        releasedPosition = Vector2D.NaN;
        animationPosition = Vector2D.NaN;
        scaledVector = Vector2D.NaN;
      }
      case MotionEvent.ACTION_UP => {
        releasedPosition = touchPosition;
        log log pressedPosition;
        log log releasedPosition;

        val pressedReleasedVector = releasedPosition.subtract(pressedPosition);
        log log pressedReleasedVector;

        scaledVector = pressedReleasedVector.scalarMultiply(period / speedFactor);
        log log scaledVector;

        animationPosition = Vector2D.ZERO;

        fadePaint.setAlpha(255); //completely opaque
      }
      case _ => {}
    }

    // return true is useful to tell the on touch method if it must hold on the touch or if the code should let go even if the user is still
    // touching the screen. In other words, the on touch keeps refreshing itself while the user touches the screen drags his finger
    // While if it returns false once then the on touch will not be called again until the user raises his finger from the screen and retouches it
    true;
  }

  // inline class! It has access to the properties of the parent class
  class MyBringBackSurface(private val surfaceView: SurfaceView) extends Runnable {
    private var ourThread: Option[Thread] = None;
    private var isRunning: Boolean = false;

    private lazy val ourHolder: SurfaceHolder = surfaceView.getHolder;

    private lazy val backgroundColor = getResources.getColor(R.color.background);

    private lazy val centerPos = new Vector2D(surfaceView.getWidth / 2, surfaceView.getHeight / 2);

    private lazy val bitmapDrawableBackground = {
      val bd = new BitmapDrawable(getResources,BitmapFactory.decodeResource(getResources,R.drawable.dontwait_background));
      bd.setTileModeX(Shader.TileMode.REPEAT);
      bd.setTileModeY(Shader.TileMode.REPEAT);
      bd.setBounds(0, 0, surfaceView.getWidth, surfaceView.getHeight);
      bd;
    }


    def resume() {
      isRunning = true;
      ourThread = Some(new Thread(this));
      ourThread.get.start();
    }

    def pause() {
      isRunning = false;
      try {
        //we loop but we would like to wait for a loop to finish before continuing.
        //this is where join helps us. join is a blocking call. will end when execution finishes
        ourThread.get.join();
      }
      catch {
        case e: InterruptedException => {
          e.printStackTrace();
        }
      }
      ourThread = None;
    }

    // Here we run our canvas painting behind in a new thread so we do not get heavy on the ui thread!
    def run() {
      while (isRunning) {
        val startTime = System.currentTimeMillis();

        if (ourHolder.getSurface.isValid) {
          val canvas = ourHolder.lockCanvas();

          if (touchPosition.isNaN) {
            touchPosition = centerPos;
          }

          //canvas.drawColor(Color.BLACK);
          //val hello = new Color();
          //val mycolor = Color.parseColor("#FFFFFF");
          //canvas.drawRGB(2,2,150);

          //canvas.drawColor(backgroundColor);
          bitmapDrawableBackground.draw(canvas);

          //touchPosition.equals(Vector2D.ZERO);
          //if (x != 0 && y != 0) {
          canvas.drawBitmap(
            greenball,
            touchPosition.getX.toFloat - greenball.getWidth / 2,
            touchPosition.getY.toFloat - greenball.getHeight / 2,
            null // null painter because we do not need to paint an already existing png image ;)
          );
          //}

          if (!releasedPosition.isNaN) {

            val newAlpha = fadePaint.getAlpha - (fadeFactor / period);
            fadePaint.setAlpha(if (newAlpha >= 0) newAlpha else 0);

            canvas.drawBitmap(
              greenball,
              releasedPosition.getX.toFloat - greenball.getWidth / 2 + animationPosition.getX.toFloat,
              releasedPosition.getY.toFloat - greenball.getHeight / 2 + animationPosition.getY.toFloat,
              fadePaint
              //null // null painter because we do not need to paint an already existing png image ;)
            );

            animationPosition = animationPosition.add(scaledVector);
          }

          ourHolder.unlockCanvasAndPost(canvas);
        }

        val sleepDuration = period - (System.currentTimeMillis() - startTime);

        // this is for the frames per second.
        // making our thread sleep a little bit is making our application consume less resources
        if (sleepDuration > 0) {
          try {
            Thread.sleep(sleepDuration);
          }
          catch {
            case e: InterruptedException => {
              e.printStackTrace();
            }
          }
        }
      }
    }
  }

}
